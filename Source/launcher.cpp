
#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QtQml>

#include "launcher.hpp"

Launcher::Launcher()
{

}

bool Launcher::launch(const QString& name)
{
    if (name.isEmpty())
        return false;
    QString program = name;
    QStringList arguments;
    arguments << "";
    qDebug() << "Exists" << QFile::exists(program) << program;

    QProcess *myProcess = new QProcess(nullptr);
    myProcess->start(program, arguments);

    return true;
}

