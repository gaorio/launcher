#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>

#include "launcher.hpp"
#include "appmodel.hpp"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Launcher launcher;
    QList<QObject*> appModel;
    bool removepath = false;


    QString currentpath = QDir::currentPath();

    if(currentpath.endsWith("/binwin"))
        removepath = true;
    if(removepath)
        currentpath = currentpath.remove(currentpath.lastIndexOf("/"), currentpath.count());
    QDirIterator dirList(currentpath, QDirIterator::NoIteratorFlags);

    while (dirList.hasNext()) {
        QDirIterator scriptList(dirList.next(), QStringList() << "*.ico", QDir::Files, QDirIterator::NoIteratorFlags);
        while (scriptList.hasNext()) {
            QString iconScript = scriptList.next();
            QString nameScript = scriptList.fileName();
            QString descScript = scriptList.fileName();
            QString exeScript = scriptList.path();
            QString helpScript = "";

            nameScript = nameScript.replace(".ico", ".exe");
            nameScript = exeScript + "/binwin/" + nameScript;

            QFile file(exeScript + "/Name.txt");
            if(QFileInfo::exists(exeScript + "/Name.txt"))
            {
                file.open(QIODevice::ReadOnly | QIODevice::Text);
                descScript = file.readAll();
                file.close();
            }
            else
            {
                descScript = "";
            }

            file.setFileName(exeScript + "/ExePath.txt");
            if(QFileInfo::exists(exeScript + "/ExePath.txt"))
            {
                file.open(QIODevice::ReadOnly | QIODevice::Text);
                nameScript = file.readAll();
                file.close();
            }

            file.setFileName(exeScript + "/Help.txt");
            if(QFileInfo::exists(exeScript + "/Help.txt"))
            {
                file.open(QIODevice::ReadOnly | QIODevice::Text);
                helpScript = file.readAll();
                file.close();
            }

            qDebug() << nameScript << " -> " << iconScript << "->" << exeScript;
            appModel.append(new AppModel(descScript, nameScript, iconScript, helpScript));
        }
    }
    QQmlApplicationEngine engine;

    engine.rootContext()->setContextProperty("launcher", &launcher);
    engine.rootContext()->setContextProperty("appModel", QVariant::fromValue(appModel));
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    int rr = app.exec();

    return rr;
}
