#ifndef LAUNCHER_HPP
#define LAUNCHER_HPP

#include <QObject>
#include <QFile>
#include <QTextStream>

class Launcher : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE bool launch(const QString& name);

public:
    Launcher();
};

#endif // LAUNCHER_HPP
