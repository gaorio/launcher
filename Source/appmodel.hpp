#ifndef APPMODEL_HPP
#define APPMODEL_HPP

#include <QObject>

class AppModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString appName READ appName CONSTANT)
    Q_PROPERTY(QString appDesc READ appDesc CONSTANT)
    Q_PROPERTY(QString appImage READ appImage CONSTANT)
    Q_PROPERTY(QString appHelp READ appHelp CONSTANT)

public:
    AppModel(QString desc, QString name, QString path, QString help = "");

    QString appName() const;
    QString appDesc() const;
    QString appImage() const;
    QString appHelp() const;
private:
    QString m_name;
    QString m_desc;
    QString m_image;
    QString m_help;
};

#endif // APPMODEL_HPP
