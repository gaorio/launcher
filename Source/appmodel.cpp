

#include <QObject>
#include <QFileInfo>
#include <QDebug>
#include "appmodel.hpp"


AppModel::AppModel(QString desc, QString name, QString path, QString help)
{
    QFileInfo(path).absolutePath();
    m_name = name;
    m_desc = desc;
    m_image = "file:" + path;
    m_help = help;
}

QString AppModel::appName() const
{
    return m_name;
}

QString AppModel::appDesc() const
{
    return m_desc;
}

QString AppModel::appImage() const
{
    return m_image;
}

QString AppModel::appHelp() const
{
    return m_help;
}



