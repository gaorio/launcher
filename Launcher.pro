TEMPLATE = app

QT += qml quick widgets

CONFIG += c++11

SOURCES += \
    Source/main.cpp \
    Source/launcher.cpp \
    Source/appmodel.cpp

HEADERS += \
    Source/launcher.hpp \
    Source/appmodel.hpp

RESOURCES += \
    ./Resource/qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES +=

lupdate_only {
    SOURCES += Resource
}
