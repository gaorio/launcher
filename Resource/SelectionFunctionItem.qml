import QtQuick 2.4

Rectangle {
    id: root

    property url imageSource
    property url imageSourcePressed
    property string txtInfoText: "text"
    property bool navigation
    property int itemWidth: 200
    property int itemHeight: 200
    property int itemSpace: 0
    property bool isEnabled: true
    property bool isPresentHelp: true

    signal clicked()
    signal clickedHelp()

    width: itemWidth - 2*itemSpace
    height: itemHeight - 2*itemSpace
    border.width: 1
    border.color: "silver"
    color: "white"

    x: itemSpace
    y: itemSpace

    Defaults {
        id: defaults
    }

    Rectangle {
        id: myRect

        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        color: "transparent"

        Rectangle {
            id: myHelp
            width: 40
            height: 40
            anchors.right: parent.right
            anchors.rightMargin: 2
            anchors.top: parent.top
            anchors.topMargin: 2
            border.width: 2
            z : 10
            visible: isPresentHelp
            enabled: isPresentHelp


            Text {
                text: "?"
                anchors.centerIn: parent
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: defaults.functionSelectionPixelSize
                font.capitalization: Font.SmallCaps
                wrapMode: Text.WordWrap
            }
            MouseArea {
                id: myAreaHelp
                anchors.fill: parent
                onClicked: {
                    root.clickedHelp()
                }
                onEntered: {
                    myHelp.border.color = "red"
                }
                onExited: {
                    myHelp.border.color = "brack"
                }
            }
        }

        Image {
            id: myImg
            width: 100
            height: 100
            anchors.top: txtInfo.text != "" ? parent.top : undefined
            anchors.topMargin: 2
            anchors.leftMargin: 40
            anchors.rightMargin: 40
            anchors.horizontalCenter: txtInfo.text != "" ? parent.horizontalCenter : undefined
            anchors.centerIn: txtInfo.text != "" ? undefined : parent
            source: imageSource
            asynchronous: true
        }

        Text {
            id: txtInfo
            width: parent.width
            anchors.left: parent.left
            anchors.top: myImg.bottom
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.leftMargin: 40
            anchors.rightMargin: 40
            text: txtInfoText
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.pixelSize: defaults.functionSelectionPixelSize
            font.capitalization: Font.SmallCaps
            wrapMode: Text.WordWrap
            color: defaults.textColor
            visible: text != ""
        }

        MouseArea {
            id: myArea
            anchors.fill: parent
            onClicked: {
                root.clicked()
            }
        }
    }

    states: [
        State {
            name: "pressed"
            when: myArea.pressed
            PropertyChanges { target: root; color: defaults.areaColor }
            PropertyChanges { target: myImg; source: imageSourcePressed }
            PropertyChanges { target: txtInfo; color: defaults.highLightColor }
        },
        State {
            name: "disabled"
            when: !isEnabled
            PropertyChanges { target: myArea; enabled: false }
            PropertyChanges { target: myRect; opacity: defaults.disabledOpacity }
        }
    ]
}
