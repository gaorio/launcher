import QtQuick 2.4

QtObject {
    id: defaults

    // COLORS
    readonly property color textColor: "#0d0d0d" // RGB 013 013 013
    readonly property color highLightColor: "#39cc84" // RGB 057 204 132
    readonly property color areaColor: "#5a5a5a" // RGB 090 090 090
    readonly property color backgroundAppColor: "#ffffff" // RGB 255 255 255

    // COMPONENTS COLOR
    readonly property color navigationBarColor: "#4f4f4f" // RGB 079 079 079

    // PIXEL SIZES
    readonly property int functionSelectionPixelSize: 18

    // OTHER
    readonly property real disabledOpacity: 0.3
}
