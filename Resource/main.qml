import QtQuick 2.4
import QtQuick.Controls 1.4

ApplicationWindow {
    id: root
    width: 1280
    height: 800
    minimumWidth: 800
    minimumHeight: 600
    title: qsTr("LAUNCHER")
    visible: true;

    Defaults {
        id: defaults
    }

    Rectangle {
        id: topbar
        anchors.top: parent.top
        width: parent.width
        height: 60
        color: defaults.navigationBarColor
        z: 1

        Text {
            text: qsTr("LAUNCHER")
            anchors.centerIn: parent
            color: defaults.backgroundAppColor
            font.pixelSize: 14
            font.bold: true
        }
    }
    Rectangle{
        id : helpRect
        height: 70
        anchors.top : topbar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        border.color: "red"
        border.width: 2
        visible: false

        Text{
            id : helpText
            width: parent.width
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            text: ""
            horizontalAlignment: Text.AlignHCenter
            //verticalAlignment: Text.AlignVCenter
            font.pixelSize: defaults.functionSelectionPixelSize
            font.capitalization: Font.SmallCaps
            wrapMode: Text.WordWrap
            color: defaults.textColor
        }
        Button{
            id : helpClose
            height: 60
            width : 150
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            text: "OK"
            onClicked: {
                helpRect.height = 70
                helpRect.visible = false
            }
        }

        z:200
    }

    Rectangle {
        id: mainRect
        anchors.left: parent.left
        anchors.top: topbar.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        anchors.bottomMargin: 5
        color: defaults.backgroundAppColor
        border.width: 2
        border.color: "yellow"

        Grid {
            id: gridFunctions;
            anchors.fill: parent
            anchors.margins: 10
            anchors.centerIn: parent
            clip: true
            enabled: visible
            spacing: cellSpacing
            columns: width / (cellWidth + cellSpacing)

            readonly property int cellSpacing:10
            readonly property int cellWidth: 280 + 10
            readonly property int cellHeight: 204

            Repeater {
                model: appModel

                delegate: SelectionFunctionItem {
                    itemWidth: gridFunctions.cellWidth
                    itemHeight: gridFunctions.cellHeight
                    itemSpace: gridFunctions.cellSpacing
                    txtInfoText: model.appDesc
                    imageSource: model.appImage
                    imageSourcePressed: model.appImage
                    isPresentHelp : model.appHelp !== ""

                    onClicked: {
                        launcher.launch(model.appName);
                        //Qt.quit()
                    }
                    onClickedHelp: {
                        helpText.text = model.appHelp
                        helpRect.height = root.height -100
                        helpRect.visible = true
                    }
                }
            }
        }
    }
}
